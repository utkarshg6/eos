var express = require('express')
var router = express.Router()
var fs = require('fs')

/* ==========================================================================
   Generate a single .json file from all the eos-icons files.
   ========================================================================== */

let data = []
const dir = `assets/javascripts/application/models/eos-icons/`

let iconsSet
fs.readdir(dir, (err, files) => {
  return new Promise((resolve, reject) => {
    if (err) reject(err)
    files.forEach(file => {
      var obj = JSON.parse(fs.readFileSync(`assets/javascripts/application/models/eos-icons/${file}`, 'utf8'))
      data.push(obj)
    })
    resolve(data)
  }).then(data => {
    fs.writeFileSync('assets/javascripts/application/models/eos-set.json', JSON.stringify(data))
  }).then(() => {
    iconsSet = JSON.parse(fs.readFileSync('assets/javascripts/application/models/eos-set.json', 'utf8'))
  })
})

/* ==========================================================================
  API ENDPOINT
========================================================================== */
/**
 *  => /api/icons                 Return all the icons
 *  => /api/icons/SEARCH          Return one of more icons based on name, tag or do/dont
 */
/* Show all icons if no query is present
  ========================================================================== */
router.get('/', function (req, res, next) {
  res.status(200).json(iconsSet)
})

/* Return a parced response object
  ========================================================================== */
router.get('/:search', function (req, res, next) {
  let reqLower = req.params.search.toLocaleLowerCase()

  let multipleWordsQuery = reqLower.split(' ')
  /* ==========================================================================
     Temporary (_) array that holds our request response.
     ========================================================================== */
  let _response = []
  // Update array with the item position that match the query
  function upadateResponse (i) {
    _response.push(iconsSet[i])
  }

  /* ==========================================================================
    Funtion to filter content.
    ========================================================================== */
  function filterIcons (search) {
    for (let i = 0; i < iconsSet.length; i++) {
      // Create a new array with words from the icons. Ej: eos_chains = ['eos', 'chains']
      let _arrWords = iconsSet[i].name.split(['_'])
      let selectTags = iconsSet[i].tags

      // Check for tags
      for (let y = 0; y < selectTags.length; y++) {
        // Split the multiple words tag in an array of words.
        let iconTags = selectTags[y].split(/\s/)
        // If the word is present in one of the tag, show that icon.
        if (iconTags.find(tag => tag === search)) {
          upadateResponse(i)
        }
      }

      // Check the do for each icon, clear the HTML from the string and convert everthing to lowercase.
      let _do = iconsSet[i].do.replace(/<[^>]+>/ig, '')
      _do.split(/\s+|\./ig)
      let iconDo = _do.toLocaleLowerCase()

      // Check the dont for each icon, clear the HTML from the string and convert everthing to lowercase.
      let _dont = iconsSet[i].dont.replace(/<[^>]+>/ig, '')
      _dont.split(/\s+|\./ig)
      let iconDont = _dont.toLocaleLowerCase()

      // If the icon title, do or dont match the query, display icons.
      for (let y = 0; y < _arrWords.length; y++) {
        if (search === _arrWords[y] || iconDo.includes(search) || iconDont.includes(search)) {
          upadateResponse(i)
        }
      }
    }
  }

  /* ==========================================================================
    Call the filter function for each word in querry
    ========================================================================== */
  multipleWordsQuery.map((word) => {
    filterIcons(word)
  })

  /* ==========================================================================
     If the search by name/tag return no match, throw a 404 error.
     ========================================================================== */
  if (_response.length === 0) return res.status(404).json({ error: `No results found` })

  /* ==========================================================================
     Filter the _response looking for duplicates.
     ========================================================================== */
  /**
   * Since the tag filter can duplicate the array because more than one tag
   * contain the same keyword, before we send the array we clear the duplicates.
   * https://stackoverflow.com/questions/2218999/remove-duplicates-from-an-array-of-objects-in-javascript
   */

  let response = _response.filter((tag, index, self) =>
    index === self.findIndex((t) => (
      t.place === tag.place && t.name === tag.name
    ))
  )

  res.status(201).json(response)
})

module.exports = router
