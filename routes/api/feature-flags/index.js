var express = require('express')
var router = express.Router()
const { generateFlagsFile } = require('../../../modules/feature-flags')

router.get('/', function (req, res) {
  generateFlagsFile()
  res.send('File was generated, you can close this windows now =)')
})

module.exports = router
