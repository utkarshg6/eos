var express = require('express')
var router = express.Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('writing/brand-voice', {
    title: 'Writing guides: Brand Voice | EOS',
    description: 'A brand\'s Voice is unique. It reflects who we are, our personality. It needs to be consistent across our applications in order for users to feel it\'s SUSE speaking.',
    path: req.originalUrl
  })
})

module.exports = router
