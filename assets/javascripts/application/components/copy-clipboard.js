/* Copy to clipboard
   ========================================================================== */
$(function () {
  /**
  * Add data-attr to the js-cc class.
  * cc = copy clipboard
  * use it as:
  * .element.js-cc[data-clipboard-text='text to copy to clipboard']
  */
  $('.js-cc').attr({'title': 'Copied to clipboard', 'data-toggle': 'tooltip'})

  $('.js-cc').on('click', copyToClipboard)

  /* Bootstrap 3 tooltip setup */
  $("[data-toggle='tooltip']").tooltip({
    trigger: 'click',
    placement: 'top'
  })
})

function copyToClipboard () {
  $(this).attr('data-toggle', 'tooltip')
  const value = $(this).data('clipboard-text')

  /* Hide tooltip after click */
  $(this).mouseout(function () {
    $("[data-toggle='tooltip']").tooltip('hide')
  })

  /* Temporarily create a wrap for the value to copy */
  const _temp = $('<input>')
  $('body').append(_temp)
  _temp.val(value).select()
  document.execCommand('copy')
  _temp.remove()
}
