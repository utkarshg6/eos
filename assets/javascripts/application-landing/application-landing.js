/* always initiate the scroll at the top */
$(window).on('beforeunload', function () {
  $(window).scrollTop(0)
})

/* init WOW.js */
const wow = new WOW( // eslint-disable-line no-undef
  {
    offset: 30
  }
)
wow.init()
