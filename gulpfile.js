/* ==========================================================================
   Gulp - Builder manager for the Front-end
   ========================================================================== */

/* Configuration variables
   ========================================================================== */

// set dependencies required for the tasks
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  filter = require('gulp-filter'),
  rename = require('gulp-rename'),
  gulpMain = require('./modules/gulp-main.js'),
  clean = require('gulp-clean'),
  minify = require('gulp-minify'),
  cleanCSS = require('gulp-clean-css')

// set the folders to read and inject vendor files
var destination = 'vendors/build/',
  origin = 'vendors/'

// Set the filters
var jsFilter = filter('**/*.js'),
  cssFilter = filter('**/*.css'),
  // we need to filter out MD fonts as it will have its own filter
  fontFilter = filter(['**/*.{otf,eot,svg,ttf,woff,woff2}', '!**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}'], {restore: true})
  mdIconsFilter = filter('**/MaterialIcons-Regular.{otf,eot,svg,ttf,woff,woff2}')

/* Building tasks
   ========================================================================== */

// Extract all css files declared in the mainfiles object
gulp.task('extract:css', function () {
  return gulp.src(gulpMain(origin))
    .pipe(cssFilter)
    .pipe(concat('vendors.css'))
    .pipe(cleanCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(destination + '/css'))
})

// Extract all js files declared in the mainfiles object
gulp.task('extract:js', function () {
  return gulp.src(gulpMain(origin))
    .pipe(jsFilter)
    .pipe(concat('vendors.js'))
    .pipe(minify({ext:{
        min:'.min.js'
      }}))
    .pipe(gulp.dest(destination + '/js'))
})

// Extract all font files declared in the mainfiles object
gulp.task('extract:fonts', function () {
  return gulp.src(gulpMain(origin))
    .pipe(fontFilter)
    .pipe(gulp.dest(destination + '/fonts')) // move all fonts, except for MD icons to the /fonts folder as per fontawesome and eos-icons default configuration
    .pipe(fontFilter.restore)
    .pipe(mdIconsFilter)
    .pipe(gulp.dest(destination + '/css')) // Material icons .css file is configured to have the css and fonts in the same folder
})

// Clean the built folder to start a fresh building
gulp.task('clean', function () {
  return gulp.src(destination, {read: false})
    .pipe(clean());
});

/* Tarks to move the vendros .min files */
gulp.task('move:bootstrapMap', function () {
  return gulp.src('vendors/node_modules/bootstrap/dist/css/bootstrap.min.css.map')
      .pipe(gulp.dest(destination + 'css'));
})

gulp.task('move:jqueryMaps', function () {
  return gulp.src('vendors/node_modules/jquery/dist/jquery.min.map')
      .pipe(gulp.dest(destination + 'js'));
})

// global extract tasks
gulp.task('extract:all', [
  'extract:css',
  'extract:js',
  'extract:fonts',
  'move:bootstrapMap',
  'move:jqueryMaps'
  ]
);

// Default task
gulp.task('default', ['clean'], function () {
  gulp.start('extract:all');
});
