var fs = require('fs')
const fetch = require('node-fetch')
var configData = JSON.parse(fs.readFileSync('./config.json'), 'utf8')


/* Initial middleware based on config file */
const projectConfig = async(req, res, next) => {

  /* Define Querys for GraphQL */
  const appQuery = `application(id: "5c6fb27500382c04d13c5ef6"){name brand showLanding showChangelog enableFeedback enableGTM}, pages{title, description, path}`

  /* Get query result */
  const app = await strapiGraphql(appQuery)
  const { showLanding, showChangelog } = app.data.application

  /* Reduce the route response to new object */
  const routes = await app.data.pages.reduce((acc, cur) => {
    /* acc = accumulator , cur = current item */
    acc[cur.path] = {
      title: cur.title,
      description: cur.description
    }

    return acc
  }, {})

  /* Remeber the path for future implementation */
  const requestRoute = req.originalUrl

  /* Send the object with res.locals so we can use the props inside our views. */
  res.locals.config = app.data.application
  res.locals.stylesheetURL = configData.colors.stylesheetURL

  /* When the request is made, we check if the route corresponde with one of our defined routes, if they exist we create the locals for title and description. If we declare them outside this condition the API's would break */
  if (routes[requestRoute]) {
    const { title, description } = routes[requestRoute]

    res.locals.title = title
    res.locals.description = description
    res.locals.path = requestRoute
  }

  /* Handle redirection to dashboards for Landing & Changelog pages  */
  if ((requestRoute === '/' && showLanding === false) || (requestRoute === '/changelog' && showChangelog === false)) {
    res.redirect('/dashboard')
  } else {
    next()
  }
}

/* Strapi graphql query result.
  ========================================================================== */
const strapiGraphql = async (query) => {
  var { strapiURL } = configData

  /* Fetch graphql with the query */
  const request = await fetch(`${strapiURL}/graphql?query={${query}}`)
  const data = request.json()

  return data
}

module.exports = projectConfig
