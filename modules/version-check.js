var semver = require('semver')
var requiredNodeVersion = process.env.npm_package_engines_node
var requiredNpmVersion = process.env.npm_package_engines_npm
var currentNodeVersion = process.version
var exec = require('child_process').exec
var consoleColor = require('colors/safe')

if (!semver.satisfies(currentNodeVersion, requiredNodeVersion)) {
  console.log(consoleColor.red(`Your current NodeJs version ${currentNodeVersion} does not meet the required version for this application: ${requiredNodeVersion}.`))
  process.exit(1)
}

exec('npm -v', function (error, stdout, stderr) {
  if (error) {
    console.log('error: ' + error + stderr)
    return
  }

  const currentNpmVersion = stdout

  if (!semver.satisfies(currentNpmVersion, requiredNpmVersion)) {
    console.log(consoleColor.red(`Your current NPM version ${currentNpmVersion} does not meet the required version for this application: ${requiredNpmVersion}. \n Please upgrade "npm i -g npm@${requiredNpmVersion}" and run "npm install --engine-strict" again`))
    process.exit(1)
  }
})
